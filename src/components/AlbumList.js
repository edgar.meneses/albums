import React,{ Component} from 'react';
import { ScrollView} from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';

class AlbumList extends Component {

    state = { albums: []};

    // Metodo que permite identificar que el componente se esta renderizando en pantalla
    componentWillMount() {
        axios.get('https://rallycoding.herokuapp.com/api/music_albums',{
            method:'GET',
            mode:'no-cors',
            headers:{
                'Access-Control-Allow-Origin':'*',
                'Content-type':'jsonp'
            }
        })
            .then( response => this.setState({albums: response.data }));
    }

    renderAlbums() {
        return this.state.albums.map( album => {
            return <AlbumDetail key={album.title} album={album}/>
        })
    }

    render() {
        return (
            <ScrollView>
                {this.renderAlbums() }
            </ScrollView>
        );
    }
}

export default  AlbumList;