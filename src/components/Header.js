// Importacion de librerias para construir el componente
import React from 'react';
import {Text, View} from 'react-native';


// Construccion del componente
const Header = (props) => {
    const {textStyle, viewStyle} = styles;
    return (
        <View style={viewStyle}>
            <Text style={textStyle}>{props.headerText}</Text>
        </View>
    );
};

// declaracion de estilos

const styles = {
    textStyle:{
        fontSize: 20
    },
    viewStyle : {
        backgroundColor: '#F8F8F8',
        alignItems:'center',
        justifyContent: 'center',
        height: 60,
        paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2},
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    }
}

// Hace que el componente este disponible en otras partes de la app
export default Header;

/*
Estilos para justificar contenido
    justifyContent:'flex-end' --> al final del contenedor en el eje y
    justifyContent:'center' --> centrado en el eje y
    justifyContent:'flex-start' --> en el origen del contenedor eje y

    alignItems: 'flex-start' en el origen del contenedor en el eje x
    alignItems: 'center' centrado en el eje x
    alignItems: 'flex-end' posicionado al final del contenedor sobre el eje x

* */